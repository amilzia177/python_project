def add_numbers(a, b):
    """
    Adds two numbers and returns the result.
    
    :param a: The first number
    :param b: The second number
    :return: The sum of the two numbers
    """
    return a + b

num1 = 88
num2 = 435
result = add_numbers(num1, num2)
print("The sum is:", result)
